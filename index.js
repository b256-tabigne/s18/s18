console.log("Hello World")

/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function

	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.

	3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.

	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
	

	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/
// ADDITION
function add(num, num2) {

	console.log("Displayed sum of " + num + " and " + num2);
	console.log(num + num2);
}

add(64, 5);

// SUBTRACTION
function sub(num3, num4) {

	console.log("Displayed difference of " + num3 + " and " + num4);
	console.log(num3 - num4);
}

sub(64, 5);


// MULTIPLICATION
const multiply = function prod(num5, num6) {

	console.log("Displayed product of " + num5 + " and " + num6 + ":");
	let prod = num5 * num6
	return prod;
}

let product = multiply(64,5);
console.log(product)


// DIVISION
const divide = function quo(num7, num8) {;

	console.log("Displayed quotient of " + num7 + " and " + num8 + ":");
	let quo = num7 / num8;
	return quo;
}

let quotient = divide(64,5);
console.log(quotient)


// Circle
const circle = function radius(rad) {;

	let pi = 3.14159;
	console.log("The result of getting the area of a circle with " + rad + " radius:");
	let radius = pi * rad * rad;
	return radius;
}
let area = circle(5);
console.log(area)


// AVERAGE
const average = function ave(number1, number2, number3, number4) {;

	console.log("The average of " + number1 + ", " + number2 + ", " + number3 + ", " + number4 + " is");
	let ave = (number1 + number2 + number3 + number4) / 4;
	return ave;
}

let totalave = average(64,56,74,51);
console.log(totalave)


// PASSING SCORE
const percentage = function pass(per, cent) {

	console.log("Is " + per + "/" + cent + " a passing score:");
	let passingscore = per / cent * 100;
	let pass = passingscore >= 75;
	return pass;
}

let passed = percentage(35, 50);
console.log(passed)